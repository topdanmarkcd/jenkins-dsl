# This repo is deprecated. It has been moved to topdanmark github

[![Build Status](https://buildhive.cloudbees.com/job/sheehan/job/job-dsl-gradle-example/badge/icon)](https://buildhive.cloudbees.com/job/sheehan/job/job-dsl-gradle-example/)

# Jenkins Top DSL

This project contains core functionality and examples on how to use jenkins DSL in a Topdanmark context.

## File structure

    .
    ├── jobs                    # DSL script files that can be loaded automatically on jenkins at startup.
    │                           # This requires a seed job to be set up. See "Seed Job" 
    ├── examples                # Examples. Currently from the repo we cloned from. Should be augmented with more
    │                           # examples as we go along.
    ├── resources               # resources for DSL scripts
    ├── src
    │   ├── main
    │   │   ├── groovy          # support classes
    │   │   └── resources
    │   │       └── idea.gdsl   # IDE support for IDEA
    │   └── test
    │       └── groovy          # specs
    └── build.gradle            # build file

# Script Examples

* [Example 1](jobs/example1Jobs.groovy) - shows basic folder/job creation
* [Example 2](jobs/example2Jobs.groovy) - shows how to create a set of jobs for each github branch, each in its own folder
* [Example 3](jobs/example3Jobs.groovy) - shows how to use the configure block
* [Example 4](jobs/example4Jobs.groovy) - shows a way to reuse job definitions for jobs that differ only with a few properties
* [Example 5](jobs/example5Jobs.groovy) - shows how to pull out common components into static methods
* [Example 6](jobs/example6Jobs.groovy) - shows how to include script resources from the workspace
* [Example 7](jobs/example7Jobs.groovy) - shows how to create jobs using builders

# Links
* [job dsl plugin wiki](https://github.com/jenkinsci/job-dsl-plugin/wiki) - Wiki
* [User Power mode](https://github.com/jenkinsci/job-dsl-plugin/wiki/User-Power-Moves) - User Power mode
* [Simple example](https://github.com/sheehan/job-dsl-gradle-example) - Simple example
* [Playground](http://job-dsl.herokuapp.com/) - Playground
* [XXX](https://URL) - NEXT
* [XXX](https://URL) - NEXT

# IDE's
This is reported to work with most IDE's.
I (Jesper W) started out with eclipse, but it did not work out of the box, so I  tool intellij (community edition) instead and 
that worked out of the box.
Lars Nymand uses Netbeans - that seems to work as well

## Completion in IDE's
To get completion on the dsl, you need a specific version of _jenkins-core_.
In gradle it looks like this

repositories {
    maven { url 'http://repo.jenkins-ci.org/releases/' }
}

dependencies {
    compile 'org.jenkins-ci.main:jenkins-core:1.609.3'
}

## Seed Job

You can create the example seed job via the Rest API Runner (see below) using the pattern `jobs/seed.groovy`.

Or manually create a job with the same structure:

* Invoke Gradle script → Use Gradle Wrapper: `true`
* Invoke Gradle script → Tasks: `clean test`
* Process Job DSLs → DSL Scripts: `jobs/**/*Jobs.groovy`
* Process Job DSLs → Additional classpath: `src/main/groovy`
* Publish JUnit test result report → Test report XMLs: `build/test-results/**/*.xml`

## REST API Runner

A gradle task is configured that can be used to create/update jobs via the Jenkins REST API, if desired. Normally
a seed job is used to keep jobs in sync with the DSL, but this runner might be useful if you'd rather process the
DSL outside of the Jenkins environment or if you want to create the seed job from a DSL script.

```./gradlew rest -Dpattern=<pattern> -DbaseUrl=<baseUrl> [-Dusername=<username>] [-Dpassword=<password>]```

* `pattern` - ant-style path pattern of files to include
* `baseUrl` - base URL of Jenkins server
* `username` - Jenkins username, if secured
* `password` - Jenkins password or token, if secured

## Testing

* `./gradlew test` runs the specs.

[JobScriptsSpec](src/test/groovy/com/dslexample/JobScriptsSpec.groovy) 
will loop through all DSL files and make sure they don't throw any exceptions when processed.