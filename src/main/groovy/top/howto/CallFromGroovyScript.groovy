package top.howto

/**
 * Created by jesper on 10/22/15.
 *
 * This example shows how to call a in this project from a groovy script in jenkins
 *
 *
 */
class CallFromGroovyScript {
    // You have to provide the 'out' object from the groovy script if you want to say something
    //
    // Inside Jenkins, the script looks like this:
    //
    // ---------------------------------------------
    //    import top.scratch.CallFromGroovyScript
    //
    //   This is a static call that should return 0
    //    println 'DSL script in jenkins seed2 job called'
    //
    //    CallFromGroovyScript.hello(out, 'This is class CallFromGroovyScript speaking')    //
    // ---------------------------------------------
    // Has (in jenkins)
    // 'Additional classpath' under 'process job dsl -> advanced'
    // set to
    // 'src/main/groovy'
    //
    // And outputs:
    //    DSL script in jenkins seed2 job called
    //    This is class CallFromGroovyScript speaking
    // ---------------------------------------------
    //
    // So - if you call a class from groovy script code in jenkins, and you want it to print something to
    //      the jenkins build job log - you have to pass in a parameter.

    public static hello(PrintStream out, String message) {
        out?.println(message)
    }
}
