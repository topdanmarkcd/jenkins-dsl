package top.howto

import javaposse.jobdsl.dsl.DslFactory
import javaposse.jobdsl.dsl.Job

/**
 * Created by jesper on 10/22/15.
 */
class BuildFramework {
    public static Job basicJob(DslFactory dsl, String name) {
        def myJob = dsl.job(name) {
            description("This job is created by groovy code in the dsl project")
        }
        return myJob
    }

}
