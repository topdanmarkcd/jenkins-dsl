package com.dslexample

import jenkins.model.Jenkins


/**
 * Created by jesper on 10/21/15.
 */
class MyFirstGroovyClass {
    def someMethod() {
        println Jenkins.instance.projects.collect { it.name }
    }
}
